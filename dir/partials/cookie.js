function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
    } else {
        user = prompt("Entrez votre nom:", "");
        var random = getRandomInt(1, 10000);
        var date = Date.now()
        user = user + date;
        user = user + random;
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}

function masquer_div(id)
{
    var user = getCookie("username");
    if (user != "") {
            document.getElementById('ded').innerHTML=user;
        if (document.getElementById(id).style.display == 'none')
                document.getElementById(id).style.display = 'block';
        else 
            document.getElementById(id).style.display = 'none';
    }
}